const Joi = require('joi');
const mongoose = require('mongoose');

const truckSchema = mongoose.Schema({

  created_by: {
    type  : String
  },
  assigned_to: {
    type: String,
    default: null
  },
  type: {
    type: String,
    required: true,
  },
  status: {
    type: String
  },
  created_date: {
    type: Date,
    default: Date.now(),
  },
},
{
  versionKey: false,
});

module.exports.Truck = mongoose.model('Truck', truckSchema);
