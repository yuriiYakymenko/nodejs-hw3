const mongoose = require('mongoose');

const loadSchema = mongoose.Schema({

  created_by: {
    type  : String
  },
  assigned_to: {
    type: String,
  },
  status: {
    type: String
  },
  state: {
    type: String
  },
  name: {
    type: String,
    required: true
  },
  payload: {
    type: Number,
    required: true
  },
  pickup_address: {
    type: String,
    required: true
  },
  delivery_address: {
    type: String,
    required: true
  },
  dimensions: {
    width: {
      type: Number,
      required: true
    },
    length: {
      type: Number,
      required: true
    },
    height: {
      type: Number,
      required: true
    }
  },
  logs: [{
    message: String,
    time: {
      type: Date,
      default: Date.now()
    }
  }],
  created_date: {
    type: Date,
    default: Date.now(),
  },
},
{
  versionKey: false,
});

module.exports.Load = mongoose.model('Load', loadSchema);