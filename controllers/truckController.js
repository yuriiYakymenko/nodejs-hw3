const {Truck} = require('../models/truckModel');

module.exports.createTruck = async (req, res) => {
  const {type} = req.body;
  const {_id} = req.user;

  const truck = new Truck({
    created_by: _id,
    type,
    status: 'IS'
  });

  await truck.save();
  res.status(200).json({message: 'Truck created successfully'});
};

module.exports.getTrucks = async (req, res) => {
  const {_id} = req.user;
  
  const trucks = await Truck.find({created_by: _id});
  if (!trucks) {
    return res.status(400).json({message: 'Trucks were not found'});
  }
  res.status(200).json({trucks});
};

module.exports.getTruckById = async (req, res) => {
  const _id = req.params.id;
  const driverId = req.user._id;

  const truck = await Truck.findOne().and([{_id}, {created_by: driverId}]);
  if (!truck) {
    return res.status(400).json({message: 'Truck was not found'});
  }

  res.status(200).json({truck});
};

module.exports.updateTruck = async (req, res) => {
  const _id = req.params.id;
  const type = req.body.type;
  const driverId = req.user._id;

  if (!(await Truck.findOneAndUpdate({type}).and([{_id}, {created_by: driverId}]))) {
    return res.status(400).json({message: 'Truck was not found'});
  }

  res.status(200).json({message: 'Truck details changed successfully'});
};

module.exports.deleteTruck = async (req, res) => {
  const _id = req.params.id;
  const driverId = req.user._id;

  const truck = await Truck.findOneAndRemove().and([{_id}, {created_by: driverId}]);
  if (!truck) {
    return res.status(400).json({message: 'Truck was not found'});
  }

  res.status(200).json({message: 'Truck deleted successfully'});
};

module.exports.assignTruck = async (req, res) => {
  const _id = req.params.id;
  const driverId = req.user._id;
  
  if ((await Truck.findOne({assigned_to: driverId}))) {
    return res.status(400).json({message: 'You already have assigned truck'});
  } else if (!(await Truck.findOneAndUpdate({assigned_to: driverId}).and([{_id}, {created_by: driverId}]))) {
    return res.status(400).json({message: 'Truck was not found'});
  }

  res.status(200).json({message: 'Truck assigned successfully'});
}
