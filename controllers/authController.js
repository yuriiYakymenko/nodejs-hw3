const bcrypt = require('bcrypt');
const jwt = require('jsonwebtoken');
const {JWT_SECRET} = require('../config');

const {User} = require('../models/userModel');

module.exports.registration = async (req, res) => {
  const {email, password, role} = req.body;

  const user = new User({
    email,
    password: await bcrypt.hash(password, 10),
    role
  });
  await user.save();

  res.status(200).json({message: 'Success'});
};

module.exports.login = async (req, res) => {
  const {email, password} = req.body;

  const user = await User.findOne({email});

  if (!user) {
    return res.status(400)
        .json({message: `No user with email '${email}' found!`});
  }

  if (!(await bcrypt.compare(password, user.password))) {
    return res.status(400).json({message: `Wrong password!`});
  }

  const token = jwt.sign({email: user.email, _id: user._id, role: user.role}, JWT_SECRET);
  res.json({jwt_token: token});
};

module.exports.forgotPassword = async (req, res) => {
  const {email} = req.body;

  const user = await User.findOne({email});

  if (!user) {
    return res.status(400)
        .json({message: `No user with email '${email}' found!`});
  }
  res.status(200).json({message: "New password sent to your email address"});
};
