const bcrypt = require('bcrypt');
const {User} = require('../models/userModel');

module.exports.getInfo = async (req, res) => {
  const {email, _id} = req.user;

  const user = await User.findOne({email});
  if (!user) {
    return res.status(400).json({message: 'User was not found'});
  }
  res.status(200).json({user: {
    _id,
    email: user.email,
    created_date: user.created_date,
  },
  });
};

module.exports.deleteUser = async (req, res) => {
  const email = req.user.email;

  if ( !(await User.findOneAndDelete({email})) ) {
    return res.status(400).json({message: 'User was not found'});
  };
  res.status(200).json({message: 'Success'});
};

module.exports.changePassword = async (req, res) => {
  const {newPassword, oldPassword} = req.body;
  const email = req.user.email;
  const user = await User.findOne({email});

  if (!user) {
    res.status(400).json({message: 'User was not found'});
  }
  if ( !(await bcrypt.compare(oldPassword, user.password)) ) {
    return res.status(400).json({message: `Wrong password!`});
  }

  const newPasswordHash = await bcrypt.hash(newPassword, 10);
  const query = {'password': user.password};
  await User.findOneAndUpdate(query, {password: newPasswordHash});
  res.status(200).json({message: 'Success'});
};

