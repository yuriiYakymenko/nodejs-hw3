const {Load} = require('../models/loadModel');
const {Truck} = require('../models/truckModel');
const {TRUCKS_TYPE} = require('../config');

module.exports.createLoad = async (req, res) => {
  const {name, payload, pickup_address, delivery_address, dimensions} = req.body;
  const {_id} = req.user;

  const load = new Load({
    created_by: _id,
    status: 'NEW',
    name, 
    payload, 
    pickup_address, 
    delivery_address, 
    dimensions,
    logs: [{message: 'Load created'}]
  });

  await load.save();
  res.status(200).json({message: 'Load created successfully'});
};

module.exports.getLoadById = async (req, res) => {
  const _id = req.params.id;
  const userId = req.user._id;

  const load = await Load.findOne().and([{_id}, {created_by: userId}]);
  if (!load) {
    return res.status(400).json({message: 'Load was not found'});
  }

  res.status(200).json({load});
};

module.exports.updateLoad = async (req, res) => {
  const _id = req.params.id;
  const shipperId = req.user._id;
  const {name, payload, pickup_address, delivery_address, dimensions} = req.body;
  if (!(await Load.findOneAndUpdate({name, payload, pickup_address, delivery_address, dimensions}).and([{_id}, {created_by: shipperId}, {status: 'NEW'}]))) {
      return res.status(400).json({message: 'Load was not found'});
  }

  res.status(200).json({message: 'Load details changed successfully'});
};

module.exports.deleteLoad = async (req, res) => {
  const _id = req.params.id;
  const shipperId = req.user._id;
  if (!(await Load.findOneAndDelete().and([{_id}, {created_by: shipperId}, {status: 'NEW'}]))) {
      return res.status(400).json({message: 'Load was not found'});
  }

  res.status(200).json({message: 'Load details changed successfully'});
};

module.exports.postLoad = async (req, res) => {
  const _id = req.params.id;
  const shipperId = req.user._id;
  let load = await Load.findOneAndUpdate({status: 'POSTED'}).and([{_id}, {created_by: shipperId}, {status: 'NEW'}]);
  if (!load) {
    return res.status(400).json({message: 'Load was not found'});
  }
  const trucks = await Truck.find({status: 'IS'}).where('assigned_to').ne(null);
  let truck = [];
  for (let i = 0; i < trucks.length; i++) {
    if (
      TRUCKS_TYPE[trucks[i].type].height > load.dimensions.height &&
      TRUCKS_TYPE[trucks[i].type].width > load.dimensions.width &&
      TRUCKS_TYPE[trucks[i].type].length > load.dimensions.length &&
      TRUCKS_TYPE[trucks[i].type].payload > load.payload
    ) {
      truck = trucks[i];
      break;
    }
  }
  if (truck.length != 0) {
    await Truck.findByIdAndUpdate(truck._id, {status: 'OL'});
    await Load.findByIdAndUpdate(_id, {
      $set: {
        status: 'ASSIGNED',
        state: 'En route to Pick Up',
        assigned_to: truck.assigned_to,
        logs: [
          {
            message: 'Load was assigned to driver'
          }
        ],
      }
    });
    return res.status(200).json({message: 'Load posted successfully',
    driver_found: true});
  } else {
    await Load.findByIdAndUpdate(_id, {
      $set: {
        status: 'NEW',
        logs: [
          {
            message: 'Driver is not found'
          }
        ]
      }
    });
    return res.status(200).json({message: 'Load was not posted successfully',
    driver_found: false});
  }
};



module.exports.getLoads = async (req, res) => {
  const {_id, role} = req.user;
  const {status, limit, offset} = req.query;
  let loads;
  console.log('Status ', status, 'Limit: ', limit, 'Offset: ', offset);
  if (role === 'DRIVER') {
    if (status) {
      const loads = await Load.find({assigned_to: _id, status}, [],
      {
        skip: Number(offset),
        limit: Number(limit)
      }
      );
      return res.status(200).json({loads});
    } else {
      const loads = await Load.find({assigned_to: _id}, [],
      {
        skip: Number(offset),
        limit: Number(limit)
      }
      );
      return res.status(200).json({loads});
    }
  } else if (role === 'SHIPPER') {
    if (status) {
      const loads = await Load.find({created_by: _id, status}, [],
      {
        skip: Number(offset),
        limit: Number(limit)
      }
      );
      return res.status(200).json({loads});
    } else {
      const loads = await Load.find({created_by: _id}, [],
      {
        skip: Number(offset),
        limit: Number(limit)
      }
      );
      return res.status(200).json({loads});
    }
  }
};

module.exports.getActiveLoad = async (req, res) => {
  const userId = req.user._id;

  const load = await Load.findOne({assigned_to: userId, status: 'ASSIGNED'});
  if (!load) {
    return res.status(400).json({message: 'Load was not found'});
  }

  res.status(200).json({load});
};

module.exports.changeState = async (req, res) => {
  const userId = req.user._id;

  let load = await Load.findOne({assigned_to: userId, status: 'ASSIGNED'});
  if (!load) {
    return res.status(400).json({message: 'Load was not found'});
  }
  if (load.state === 'En route to Pick Up') {
    await Load.findByIdAndUpdate(load._id, {
      $set: {state: 'Arrived to Pick Up'},
      logs: [ { message: 'Load state changed to \'Arrived to Pick Up\''} ]
    });
    res.status(200).json({message: 'Load state changed to \'Arrived to Pick Up\''});
  } else if (load.state === 'Arrived to Pick Up') {
    await Load.findByIdAndUpdate(load._id, {
      $set: {state: 'En route to delivery'},
      logs: [ { message: 'Load state changed to \'En route to delivery\''} ]
    });
    res.status(200).json({message: 'Load state changed to \'En route to delivery\''});
  } else if (load.state === 'En route to delivery') {
    await Load.findByIdAndUpdate(load._id, {
      $set: {
        state: 'Arrived to delivery',
        status: 'SHIPPED',
        logs: [ { message: 'Load state changed to \'Arrived to delivery\''} ]
      }
    });
    res.status(200).json({message: 'Load state changed to \'Arrived to delivery\''});
  }
};



module.exports.getShippingInfo = async (req, res) => {
  const _id = req.params.id;
  const userId = req.user._id;
  const activeStatuses = ['NEW', 'POSTED', 'ASSIGNED'];

  const load = await Load.findOne().and([{_id}, {created_by: userId}]);
  if (!load || !activeStatuses.includes(load.status)) {
    return res.status(400).json({message: 'Load was not found'});
  }

  res.status(200).json({load});
};