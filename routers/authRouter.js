const express = require('express');
const router = new express.Router();

const {asyncWrapper} = require('./helpers');
const {validateRegistration} = require('./middlewares/validationMiddleware');
const {login, registration, forgotPassword} = require('../controllers/authController');

router.post('/register', asyncWrapper(validateRegistration),
    asyncWrapper(registration));
router.post('/login', asyncWrapper(login));
router.post('/forgot_password', asyncWrapper(forgotPassword));

module.exports = router;
