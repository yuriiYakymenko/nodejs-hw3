const express = require('express');
const router = new express.Router();

const {authMiddleware} = require('./middlewares/authMiddleware');
const {isShipperMiddleware} = require('./middlewares/isShipperMiddleware');
const {isDriverMiddleware} = require('./middlewares/isDriverMiddleware')
const { validateLoad } = require('./middlewares/validationLoadMiddleware');
const {asyncWrapper} = require('./helpers');
const {createLoad, getLoads, getLoadById, updateLoad, deleteLoad, postLoad, getShippingInfo, getActiveLoad, changeState} =
      require('../controllers/loadController');

router.post('/', authMiddleware, isShipperMiddleware, asyncWrapper(validateLoad), asyncWrapper(createLoad));
router.get('/', authMiddleware, asyncWrapper(getLoads));
router.get('/active', authMiddleware, isDriverMiddleware, asyncWrapper(getActiveLoad));
router.patch('/active/state', authMiddleware, isDriverMiddleware, asyncWrapper(changeState));
router.get('/:id', authMiddleware, asyncWrapper(getLoadById));
router.put('/:id', authMiddleware, isShipperMiddleware, asyncWrapper(validateLoad), asyncWrapper(updateLoad));
router.delete('/:id', authMiddleware, isShipperMiddleware, asyncWrapper(deleteLoad));
router.post('/:id/post', authMiddleware, isShipperMiddleware, asyncWrapper(postLoad));
router.get('/:id/shipping_info', authMiddleware, isShipperMiddleware, asyncWrapper(getShippingInfo));

module.exports = router;
