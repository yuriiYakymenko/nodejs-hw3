const Joi = require('joi');

module.exports.validateLoad = async (req, res, next) => {
  const schema = Joi.object({
    name: Joi.string(),
    payload: Joi.number(),
    pickup_address: Joi.string(),
    delivery_address: Joi.string(),
    dimensions: Joi.object(),
    status: Joi.string().valid('NEW', 'POSTED', 'ASSIGNED', 'SHIPPED'),
    state: Joi.string().valid('En route to Pick Up', 'Arrived to Pick Up', 'En route to delivery', 'Arrived to delivery')
  });

  const validation = await schema.validate(req.body);
  const {error} = validation;
  if (error) {
    return res.status(400).json({
      message: error.message,
    });
  }
  next()
};