module.exports.isDriverMiddleware = (req, res, next) => {
  
  if (!(req.user.role === 'DRIVER')) {
    return res.status(400).json({message: 'You are not a driver, you are not available to do this operation'});
  }

  next();
};