const Joi = require('joi');

module.exports.validateTruck = async (req, res, next) => {
  const schema = Joi.object({
    type: Joi.string().valid('SPRINTER', 'SMALL STRAIGHT', 'LARGE STRAIGHT')
  });

  const validation = await schema.validate(req.body);
  const {error} = validation;
  if (error) {
    return res.status(400).json({
      message: error.message,
    });
  }
  next()
};