const Joi = require('joi');

module.exports.validateRegistration = async (req, res, next) => {
  const schema = Joi.object({
    email: Joi.string()
        .email()
        .required(),

    password: Joi.string()
        .required(),

    role: Joi.string().valid('SHIPPER', 'DRIVER')
        .required()
  });

  const validation = await schema.validate(req.body);
  const {error} = validation;
  if (error) {
    return res.status(400).json({
      message: error.message,
    });
  }

  next();
};
