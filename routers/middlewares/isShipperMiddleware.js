module.exports.isShipperMiddleware = (req, res, next) => {
  
  if (!(req.user.role === 'SHIPPER')) {
    return res.status(400).json({message: 'You are not a shipper, you are not available to do this operation'});
  }

  next();
};