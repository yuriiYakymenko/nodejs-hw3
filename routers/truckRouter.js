const express = require('express');
const router = new express.Router();

const {authMiddleware} = require('./middlewares/authMiddleware');
const {isDriverMiddleware} = require('./middlewares/isDriverMiddleware')
const { validateTruck } = require('./middlewares/validationTruckMiddleware')
const {asyncWrapper} = require('./helpers');
const {createTruck, getTrucks, getTruckById, updateTruck, deleteTruck, assignTruck} =
      require('../controllers/truckController');

router.post('/', authMiddleware, isDriverMiddleware, asyncWrapper(validateTruck), asyncWrapper(createTruck));
router.get('/', authMiddleware, isDriverMiddleware, asyncWrapper(getTrucks));
router.get('/:id', authMiddleware, isDriverMiddleware, asyncWrapper(getTruckById));
router.put('/:id', authMiddleware, isDriverMiddleware, asyncWrapper(validateTruck), asyncWrapper(updateTruck));
router.delete('/:id', authMiddleware, isDriverMiddleware, asyncWrapper(deleteTruck));
router.post('/:id/assign', authMiddleware, isDriverMiddleware, asyncWrapper(assignTruck));

module.exports = router;
