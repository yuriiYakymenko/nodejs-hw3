const express = require('express');
const router = new express.Router();

const {authMiddleware} = require('./middlewares/authMiddleware');
const { isShipperMiddleware } = require('./middlewares/isShipperMiddleware');
const {asyncWrapper} = require('./helpers');
const {getInfo, deleteUser, changePassword} =
        require('../controllers/userController');

router.get('/', authMiddleware, asyncWrapper(getInfo));
router.delete('/', authMiddleware, isShipperMiddleware, asyncWrapper(deleteUser));
router.patch('/password', authMiddleware, asyncWrapper(changePassword));

module.exports = router;
